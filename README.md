# Kaun Banega Kookoororepati
##### Developer: Navin Pai

### About
Kaun Banega Kookoororepati (KBK) is a Quiz game powered by Kookoo APIs. It allows you to test your General Knowledge at increasing level of difficulty in an innovative manner. 

### Tech
It is powered by the following technology
- **Kookoo API** - for phone related functionality
- **MongoDB** - For persistent data story
- **Heroku** - For hosting the web application

### Demo
You can check out the app functionality by calling **02066899120**. The user phone number is used to authenticate the user.

### Version
1.0

### Future Work
- Tie in with SMS for getting SMS powered quizzing
- Web based Leaderboard to see the best quizzers in the country.


License
----

[WTFPL][wtfpl]

**Created for the [Kookoo Game Hackathon][kookoo]**

   [kookoo]: <http://www.venturesity.com/challenge/id/196>
   [wtfpl]: <http://www.wtfpl.net/txt/copying/>
