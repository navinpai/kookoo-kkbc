package org.hackathon;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.ozonetel.kookoo.CollectDtmf;
import com.ozonetel.kookoo.Response;
import org.bson.Document;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by navin on 2/4/16.
 */
@WebServlet(name = "kookoo", urlPatterns = {"/play"})
public class Main extends HttpServlet {
  /**
   * @param request  servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException      if an I/O error occurs
   */

  public String MONGO_URL = "mongodb://<dbstring>";
  public String MONGO_DB_NAME = "<collection>";
  public String MONGO_QUESTIONS_COLLECTION = "questions";
  public String MONGO_USERS_COLLECTION = "users";

  private Random random = new Random();

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/xml;charset=UTF-8");
    Response r = new Response();

    String event = request.getParameter("event");
    String phone;
    if (request.getParameter("cid").length() == 10)
      phone = "91" + request.getParameter("cid");
    else
      phone = request.getParameter("cid");

    if ((null != event) && event.equalsIgnoreCase("NewCall")) {
      int userLevel = getUserLastLevel(phone);
      Question nextQ = getNextQuestion(userLevel + 1, phone);
      r.addPlayText("Welcome to KBK Quiz");
      r.addPlayText("You have answered " + userLevel + " questions so far");
      r.addPlayText("Your next question is");
      r.addPlayText(nextQ.getQuestion());
      r.addPlayText("Options are");
      r.addPlayText("One " + nextQ.getOptA());
      r.addPlayText("Two " + nextQ.getOptB());
      r.addPlayText("Three " + nextQ.getOptC());
      r.addPlayText("Four " + nextQ.getOptD());
      CollectDtmf cd = new CollectDtmf(1, "#", 5);
      r.addCollectDtmf(cd);

    } else if ((null != event) && event.equalsIgnoreCase("GotDTMF")) {
      String choice = request.getParameter("data");
      Question q = getCurrentQuestionForUser(phone);
      if (Integer.parseInt(choice) == 1) {
        if(q.getCorrect() == "1"){
          r.addPlayText("Correct Answer");
        }
        else{
          r.addPlayText("Sorry, that is the wrong answer");
        }
        r.addPlayText("Thank You for Playing!");
        r.addHangup();
      } else if (Integer.parseInt(choice) == 2) {
        if(q.getCorrect() == "2"){
          r.addPlayText("Correct Answer");
        }
        else{
          r.addPlayText("Sorry, that is the wrong answer");
        }
        r.addPlayText("Thank You for Playing!");
        r.addHangup();
      } else if (Integer.parseInt(choice) == 3) {
        if(q.getCorrect() == "3"){
          r.addPlayText("Correct Answer");
        }
        else{
          r.addPlayText("Sorry, that is the wrong answer");
        }
        r.addPlayText("Thank You for Playing!");
        r.addHangup();
      } else if (Integer.parseInt(choice) == 4) {
        if(q.getCorrect() == "4"){
          r.addPlayText("Correct Answer");
        }
        else{
          r.addPlayText("Sorry, that is the wrong answer");
        }
        r.addPlayText("Thank You for Playing!");
        r.addHangup();
      }
      else {
        CollectDtmf cd = new CollectDtmf(1, "#", 5);
        cd.addPlayText("Invalid Value. Please Try Again.");
        r.addCollectDtmf(cd);
      }
    }

    String kookooResponseOutput = r.getXML();
    response.getWriter().write(kookooResponseOutput);
  }

  private MongoCollection<Document> getUsersCollection() {
    MongoClientURI uri = new MongoClientURI(MONGO_URL);

    MongoClient mongoClient = new MongoClient(uri);
    MongoDatabase database = mongoClient.getDatabase(MONGO_DB_NAME);
    return database.getCollection(MONGO_USERS_COLLECTION);
  }

  private MongoCollection<Document> getQuestionsCollection() {
    MongoClientURI uri = new MongoClientURI(MONGO_URL);

    MongoClient mongoClient = new MongoClient(uri);
    MongoDatabase database = mongoClient.getDatabase(MONGO_DB_NAME);
    return database.getCollection(MONGO_QUESTIONS_COLLECTION);
  }

  private int getUserLastLevel(String phone) {
    MongoCollection<Document> m = getUsersCollection();
    BasicDBObject whereQuery = new BasicDBObject();
    whereQuery.put("phone", phone);
    ArrayList<Document> user = m.find(whereQuery).into(new ArrayList<Document>());
    if(user.size() != 0) {
      return user.get(0).getInteger("level");
    }
    else{
      return 0;
    }
  }

  private Question getNextQuestion(int level, String phone) {
    MongoCollection<Document> m = getQuestionsCollection();
    BasicDBObject whereQuery = new BasicDBObject();
    int difficulty = level % 5;
    whereQuery.put("level", difficulty);
    ArrayList<Document> ques = m.find(whereQuery).into(new ArrayList<Document>());

    int index = random.nextInt(ques.size());

    Question q = new Question();

    q.setOptA(ques.get(index).getString("a"));
    q.setOptB(ques.get(index).getString("b"));
    q.setOptC(ques.get(index).getString("c"));
    q.setOptD(ques.get(index).getString("d"));

    int id = ques.get(index).getInteger("id");

    setIdForUser(ques.get(index).getInteger("id"), phone);

    return q;
  }

  private void setIdForUser(int id, String phone) {
    MongoCollection<Document> m = getUsersCollection();

    BasicDBObject newDocument = new BasicDBObject();
    newDocument.append("$set", new BasicDBObject().append("currentQ", id).append("phone", phone));

    BasicDBObject searchQuery = new BasicDBObject().append("phone", phone);

    m.updateOne(searchQuery, newDocument, (new UpdateOptions()).upsert(true));

  }

  private Question getCurrentQuestionForUser(String phone) {

    MongoCollection<Document> m = getQuestionsCollection();
    BasicDBObject whereQuery = new BasicDBObject();
    whereQuery.put("phone", phone);
    ArrayList<Document> ques = m.find(whereQuery).into(new ArrayList<Document>());


    m = getQuestionsCollection();
    whereQuery = new BasicDBObject();
    whereQuery.put("id", ques.get(0).getInteger("currentQ"));
    ques = m.find(whereQuery).into(new ArrayList<Document>());

    Question q = new Question();

    q.setOptA(ques.get(0).getString("a"));
    q.setOptB(ques.get(0).getString("b"));
    q.setOptC(ques.get(0).getString("c"));
    q.setOptD(ques.get(0).getString("d"));
    q.setCorrect(ques.get(0).getString("correct"));

    return q;
  }

}