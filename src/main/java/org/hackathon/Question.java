package org.hackathon;

/**
 * Created by navin on 3/26/16.
 */
public class Question {


  String question;
  String optA;
  String optB;
  String optC;
  String optD;
  String correct;

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public String getOptA() {
    return optA;
  }

  public void setOptA(String optA) {
    this.optA = optA;
  }

  public String getOptB() {
    return optB;
  }

  public void setOptB(String optB) {
    this.optB = optB;
  }

  public String getOptC() {
    return optC;
  }

  public void setOptC(String optC) {
    this.optC = optC;
  }

  public String getOptD() {
    return optD;
  }

  public void setOptD(String optD) {
    this.optD = optD;
  }

  public String getCorrect() {
    return correct;
  }

  public void setCorrect(String correct) {
    this.correct = correct;
  }

}
